# hórna

A streaming audio player for the clientside. 

### About
Wanna play music on your website? Wanna play your music on your website? Then hórna is for you, it provides a simple solution for playing music on any site!
Providing hórna with some config file telling it where to load the songs from and optionally a path to the cover file and you're good to go!

### Currently
This is being implemented for mcdj chew's website currently, so it'll be a pretty specific implementation. The generic solution will be updated after the fact but feel free to use it if you like!
